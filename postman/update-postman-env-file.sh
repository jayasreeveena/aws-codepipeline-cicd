#!/usr/bin/env bash

#This shell script updates Postman environment file with the API Gateway URL created
# via the api gateway deployment

echo "Running update-postman-env-file.sh"

api_gateway_url=`aws cloudformation describe-stacks \
  --stack-name cicd-api-stack \
  --query "Stacks[0].Outputs[?OutputKey=='Endpoint'].OutputValue" --output text`

echo "API Gateway URL:" ${api_gateway_url}

jq -e --arg apigwurl "$api_gateway_url" '(.values[] | select(.key=="apigw-root") | .value) = $apigwurl' \
  CICD_Environment.postman_environment.json > CICD_Environment.postman_environment.json.tmp \
  && cp CICD_Environment.postman_environment.json.tmp CICD_Environment.postman_environment.json \
  && rm CICD_Environment.postman_environment.json.tmp

echo "Updated CICD_Environment.postman_environment.json"

cat CICD_Environment.postman_environment.json