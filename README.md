## Automating API Testing with AWS Code Pipeline, AWS Code Build, Cloudformation and Postman

This post demonstrates the use of AWS CodeBuild, CodePipeline, Cloudformation and Postman to deploy, 
functionally test an AWS Proxy API, and view the generated reports using a new feature from 
CodeBuild called Reports. 

Please refer to the following blog post for this sample: 
https://aws.amazon.com/blogs/devops/automating-your-api-testing-with-aws-codebuild-aws-codepipeline-and-postman/
